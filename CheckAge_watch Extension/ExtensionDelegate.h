//
//  ExtensionDelegate.h
//  CheckAge_watch Extension
//
//  Created by Drew Bombard on 10/24/16.
//  Copyright © 2017 default_method. All rights reserved.
//

#import <WatchKit/WatchKit.h>

@interface ExtensionDelegate : NSObject <WKExtensionDelegate>

@end
