//
//  PersistentDataWatch.m
//  
//
//  Created by Drew Bombard on 3/1/17.
//
//

#import "PersistentDataWatch.h"

@implementation PersistentDataWatch

-(id)init {
	self = [super init];
	if (self) { }
	return self;
}


+(NSMutableDictionary *)fetchLocalData
{
	NSError *error;
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:@"Data-2.1.1.plist"];
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
 
	if (![fileManager fileExistsAtPath: path]) {
		NSString *bundle = [[NSBundle mainBundle] pathForResource:@"Data-2.1.1" ofType:@"plist"];
		[fileManager copyItemAtPath:bundle toPath: path error:&error];
	}

	NSMutableDictionary *dataArr = [[NSMutableDictionary alloc] initWithContentsOfFile: path];

//	NSLog(@"_dataArr: %@",dataArr);
//	NSLog(@"\n");
	return dataArr;
}

+(void)setLocalData:(NSString *)age type:(NSString *)type
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:@"Data-2.1.1.plist"];
	
	NSMutableDictionary *dataArr = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
	
	[dataArr setObject:age forKey:type];
	[dataArr writeToFile: path atomically:YES];
	
	NSLog(@"Saved %@ to %@:", age, type);
	NSLog(@"\n");
}


@end
