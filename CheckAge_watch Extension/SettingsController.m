//
//  SettingsController.m
//  CheckAge
//
//  Created by Drew Bombard on 3/2/17.
//  Copyright © 2017 default_method. All rights reserved.
//

#import "SettingsController.h"

@interface SettingsController ()

@end

@implementation SettingsController

- (void)awakeWithContext:(id)context
{
    [super awakeWithContext:context];
    
    // Configure interface objects here.
}

- (void)willActivate
{
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
	
	[_btnDateFormat setTitle: [DateFormat getTodayDate]];
//	_lblDateFormat.text = [DateFormat getTodayDate];
	
	// Set initial values of sliders
	[self setupSliderAlcohol];
	[self setupSliderTobacco];
}

- (void)didDeactivate
{
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}

-(void)setupSliderAlcohol
{
	//	// Max-mins for slider
	//_sliderAlcohol.minimumValue = 16;
	//_sliderAlcohol.maximumValue = 21;
	
	// Default slider value
	_sliderAlcohol.value = [[[PersistentData fetchLocalData] objectForKey:@"age_alcohol"] intValue];
	
	[self updateSliderAlcohol];
}

-(void)setupSliderTobacco
{
	//	// Max-mins for slider
	//_sliderTobacco.minimumValue = 16;
	//_sliderTobacco.maximumValue = 18;
	
	// Default slider value
	_sliderTobacco.value = [[[PersistentData fetchLocalData] objectForKey:@"age_tobacco"] intValue];
	
	[self updateSliderTobacco];
}
-(IBAction)sliderValueChangedAlcohol:(float)value
{
	// Round slider value
	int roundedValue = roundl(value);
	
	// Save data back to .plist
	[PersistentData setLocalData:[NSString stringWithFormat:@"%d", roundedValue] type:@"age_alcohol"];
	
	//NSLog(@"_dataArr: %@",[[PersistentData fetchLocalData] objectForKey:@"age_alcohol"]);
	//NSLog(@"\n");
	
	[self updateSliderAlcohol];
}
-(void)updateSliderAlcohol
{
	_dataArr = [PersistentData fetchLocalData];
	NSString *age = [_dataArr objectForKey:@"age_alcohol"];
	
	if (age == nil) {
		_sliderAlcohol.value = 21;
		age = @"21";
	} else {
		_sliderAlcohol.value = [age floatValue];
	}
	
	_lblSliderAlcohol.text = age;
}

-(void)updateSliderTobacco
{
	_dataArr = [PersistentData fetchLocalData];
	NSString *age = [_dataArr objectForKey:@"age_tobacco"];
	
	if (age == nil) {
		_sliderTobacco.value = 18;
		age = @"18";
	} else {
		_sliderTobacco.value = [age floatValue];
	}
	
	_lblSliderTobacco.text = age;
}

- (IBAction)sliderValueChangedTobacco:(float)value
{
	// Round slider value
	int roundedValue = roundl(value);
	
	// Save data back to .plist
	[PersistentData setLocalData:[NSString stringWithFormat:@"%d", roundedValue] type:@"age_tobacco"];
	
	//NSLog(@"_dataArr: %@",[[PersistentData fetchLocalData] objectForKey:@"age_tobacco"]);
	//NSLog(@"\n");
	
	[self updateSliderTobacco];
}

@end



