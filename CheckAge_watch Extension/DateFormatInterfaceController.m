//
//  DateFormatInterfaceController.m
//  CheckAge
//
//  Created by Drew Bombard on 4/13/17.
//  Copyright © 2017 default_method. All rights reserved.
//

#import "DateFormatInterfaceController.h"

@interface DateFormatInterfaceController ()

@end

@implementation DateFormatInterfaceController

- (void)awakeWithContext:(id)context
{
	[super awakeWithContext:context];
	// Configure interface objects here.
	
	_dateFormatsArr = @[@"M/d/YY", @"M/d/YYYY", @"M-d-YYYY", @"M.d.YYYY", @"MMM d,YYYY", @"d/M/YY", @"d/M/YYYY", @"d-M-YYYY", @"d.M.YYYY", @"d MMM, YYYY", @"YYYY-M-d"];
}

- (void)willActivate {
	// This method is called when watch view controller is about to be visible to user
	[super willActivate];
}

- (void)didDeactivate
{
	// This method is called when watch view controller is no longer visible
	[super didDeactivate];
}

-(IBAction)dateFormat0:(id)sender
{
	[PersistentData setLocalData:[_dateFormatsArr objectAtIndex:0] type:@"date_format"];
	[self popController];
}
-(IBAction)dateFormat1:(id)sender
{
	[PersistentData setLocalData:[_dateFormatsArr objectAtIndex:1] type:@"date_format"];
	[self popController];
}
-(IBAction)dateFormat2:(id)sender
{
	[PersistentData setLocalData:[_dateFormatsArr objectAtIndex:2] type:@"date_format"];
	[self popController];
}
-(IBAction)dateFormat3:(id)sender
{
	[PersistentData setLocalData:[_dateFormatsArr objectAtIndex:3] type:@"date_format"];
	[self popController];
}
-(IBAction)dateFormat4:(id)sender
{
	[PersistentData setLocalData:[_dateFormatsArr objectAtIndex:4] type:@"date_format"];
	[self popController];
}
-(IBAction)dateFormat5:(id)sender
{
	[PersistentData setLocalData:[_dateFormatsArr objectAtIndex:5] type:@"date_format"];
	[self popController];
}
-(IBAction)dateFormat6:(id)sender
{
	[PersistentData setLocalData:[_dateFormatsArr objectAtIndex:6] type:@"date_format"];
	[self popController];
}
-(IBAction)dateFormat7:(id)sender
{
	[PersistentData setLocalData:[_dateFormatsArr objectAtIndex:7] type:@"date_format"];
	[self popController];
}
-(IBAction)dateFormat8:(id)sender
{
	[PersistentData setLocalData:[_dateFormatsArr objectAtIndex:8] type:@"date_format"];
	[self popController];
}
-(IBAction)dateFormat9:(id)sender
{
	[PersistentData setLocalData:[_dateFormatsArr objectAtIndex:9] type:@"date_format"];
	[self popController];
}
-(IBAction)dateFormat10:(id)sender
{
	[PersistentData setLocalData:[_dateFormatsArr objectAtIndex:10] type:@"date_format"];
	[self popController];
}



@end



