//
//  InterfaceController.m
//  CheckAge_watch Extension
//
//  Created by Drew Bombard on 10/24/16.
//  Copyright © 2017 default_method. All rights reserved.
//

#import "InterfaceController.h"


@interface InterfaceController()

@end


@implementation InterfaceController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];

    // Configure interface objects here.
}

- (void)willActivate
{
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
	
	_dataArr = [PersistentData fetchLocalData];
	
	// If age minimums haven't been set yet...
	if ([[_dataArr objectForKey:@"default_locale_set"]  isEqual: @"0"]
		|| [_dataArr objectForKey:@"age_alcohol"] == nil
		|| [_dataArr objectForKey:@"age_tobacco"] == nil) {
		[self setMinimumAges];
	}
	
	_lbl_alcohol_date.text = [DateFormat getAlcoholMinDate];
	_lbl_tobacco_date.text = [DateFormat getTobaccoMinDate];
}

-(void)didDeactivate
{
	// This method is called when watch view controller is no longer visible
	[super didDeactivate];
}

-(void)setMinimumAges
{
	// Grab country
    NSString *locale = [[NSLocale preferredLanguages] objectAtIndex:0];
	locale = [locale lowercaseString];
	
	NSLog(@"locale: %@",locale);
	NSLog(@"_dataArr BEFORE: %@", _dataArr);
	
	
	/* Country specific age limits */
	// UK
	if ([locale isEqual: @"en-gb"]) {
		[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 18] type:@"age_alcohol"];
		[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 18] type:@"age_tobacco"];
	}
	
	//Canada
	if ([locale isEqual: @"en-ca"]) {
		[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 19] type:@"age_alcohol"];
		[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 19] type:@"age_tobacco"];
	}
	
	//Australia
	if ([locale isEqual: @"en-au"]) {
		[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 18] type:@"age_alcohol"];
		[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 18] type:@"age_tobacco"];
	}
	
	//Ireland
	if ([locale isEqual: @"en-ie"]) {
		[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 18] type:@"age_alcohol"];
		[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 18] type:@"age_tobacco"];
	}
	
	// If no specific country defaults set above, default to 18/21 U.S.
	if ([_dataArr objectForKey:@"age_alcohol"] == nil || [_dataArr objectForKey:@"age_tobacco"] == nil) {
		[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 18] type:@"age_alcohol"];
		[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 21] type:@"age_tobacco"];
	}
	
	
	
	// Defaults have now been set for first run.
	[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 1] type:@"default_locale_set"];
	
	
	// Reset the local data arr (in case min's were changed):
	_dataArr = [PersistentData fetchLocalData];
	
	NSLog(@"_dataArr AFTER: %@", _dataArr);
	NSLog(@"\n");
}


@end
