//
//  InterfaceController.h
//  CheckAge_watch Extension
//
//  Created by Drew Bombard on 10/24/16.
//  Copyright © 2017 default_method. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>

// Utilities
#import "DateFormat.h"

// Data
#import "PersistentData.h"


@interface InterfaceController : WKInterfaceController

@property (strong, nonatomic) IBOutlet WKInterfaceLabel *lbl_alcohol_date;
@property (strong, nonatomic) IBOutlet WKInterfaceLabel *lbl_tobacco_date;

@property (strong, nonatomic) IBOutlet WKInterfaceButton *btn_settings;

@property (strong, nonatomic) NSString *language;
@property (nonatomic, assign) int min_alcohol_age;
@property (nonatomic, assign) int min_tobacco_age;

// Local Data
@property (strong, nonatomic) NSMutableDictionary *dataArr;

@end
