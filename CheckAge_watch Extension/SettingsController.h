//
//  SettingsController.h
//  CheckAge
//
//  Created by Drew Bombard on 3/2/17.
//  Copyright © 2017 default_method. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>

#import "DateFormat.h"
#import "PersistentData.h"

@interface SettingsController : WKInterfaceController
@property (strong, nonatomic) IBOutlet WKInterfaceSlider *sliderAlcohol;
@property (strong, nonatomic) IBOutlet WKInterfaceLabel *lblSliderAlcohol;

@property (strong, nonatomic) IBOutlet WKInterfaceSlider *sliderTobacco;
@property (strong, nonatomic) IBOutlet WKInterfaceLabel *lblSliderTobacco;
@property (strong, nonatomic) IBOutlet WKInterfaceButton *btnDateFormat;

-(IBAction)sliderValueChangedAlcohol:(float)value;
-(IBAction)sliderValueChangedTobacco:(float)value;

// Local Data
@property (strong, nonatomic) NSMutableDictionary *dataArr;

@end
