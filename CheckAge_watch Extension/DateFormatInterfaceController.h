//
//  DateFormatInterfaceController.h
//  CheckAge
//
//  Created by Drew Bombard on 4/13/17.
//  Copyright © 2017 default_method. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>

#import "PersistentData.h"


@interface DateFormatInterfaceController : WKInterfaceController


-(IBAction)dateFormat0:(id)sender;
-(IBAction)dateFormat1:(id)sender;
-(IBAction)dateFormat2:(id)sender;
-(IBAction)dateFormat3:(id)sender;
-(IBAction)dateFormat4:(id)sender;
-(IBAction)dateFormat5:(id)sender;
-(IBAction)dateFormat6:(id)sender;
-(IBAction)dateFormat7:(id)sender;
-(IBAction)dateFormat8:(id)sender;
-(IBAction)dateFormat9:(id)sender;
-(IBAction)dateFormat10:(id)sender;

@property (strong, nonatomic) NSArray *dateFormatsArr;


@end
