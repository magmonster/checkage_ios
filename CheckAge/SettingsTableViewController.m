//
//  SettingsTableViewController.m
//  CheckAge
//
//  Created by Drew Bombard on 10/21/16.
//  Copyright © 2017 default_method. All rights reserved.
//

#import "SettingsTableViewController.h"

@interface SettingsTableViewController ()

@end

@implementation SettingsTableViewController


#pragma mark - App Setup
- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// Email "From" address..
	_toRecipients = [NSArray arrayWithObjects:@"support@defaultmethod.com", nil];
	
	// Set the app version beneath name
	_lblVersionNum.text = [AppStats versionNumberWithName];
	_lblCopyright.text = [NSString stringWithFormat: @"© %@ Default Method, LLC", [DateFormat getCurrentYear]];
	
	
	
	// Set initial values of sliders
	[self setupSliderAlcohol];
	[self setupSliderTobacco];
	[self setupSubstanceSegment];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
	[self setupColorTheme];
	if ([[[PersistentData fetchLocalData] objectForKey:@"animation"]  isEqual: @"1"]) {
		[_swAnimation setOn:YES animated:nil];
	} else {
		[_swAnimation setOn:NO animated:nil];
	}

	NSLog(@"animation: %@",[[PersistentData fetchLocalData] objectForKey:@"animation"]);
	NSLog(@"date_format: %@",[[PersistentData fetchLocalData] objectForKey:@"date_format"]);
	_lblDateFormat.text = [DateFormat getTodayDate];
}


#pragma mark - Table view config
//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//	return 30;
//}

//-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//	UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
//	[headerView setBackgroundColor:[UIColor clearColor]];
//	return headerView;
//}





#pragma mark - Slider
-(void)setupSliderAlcohol
{
	// Max-mins for slider
	_sliderAlcohol.minimumValue = 16;
	_sliderAlcohol.maximumValue = 21;

	// Default slider value
	_sliderAlcohol.value = [[[PersistentData fetchLocalData] objectForKey:@"age_alcohol"] intValue];
	
	[self updateSliderAlcohol];
}

-(void)setupSliderTobacco
{
	// Max-mins for slider
	_sliderTobacco.minimumValue = 16;
	_sliderTobacco.maximumValue = 21;
	
	// Default slider value
	_sliderTobacco.value = [[[PersistentData fetchLocalData] objectForKey:@"age_tobacco"] intValue];
	
	[self updateSliderTobacco];
}

-(void)updateSliderAlcohol
{
	_dataArr = [PersistentData fetchLocalData];
	NSString *age = [_dataArr objectForKey:@"age_alcohol"];
	
	if (age == nil) {
		_sliderAlcohol.value = 21;
		age = @"21";
	} else {
		_sliderAlcohol.value = [age floatValue];
	}
	
	_lblSliderAlchohol.text = age;
}

-(void)updateSliderTobacco
{
	_dataArr = [PersistentData fetchLocalData];
	NSString *age = [_dataArr objectForKey:@"age_tobacco"];
	
	if (age == nil) {
		_sliderTobacco.value = 18;
		age = @"18";
	} else {
		_sliderTobacco.value = [age floatValue];
	}
	
	_lblSliderTobacco.text = age;
}

- (IBAction)sliderValueChangedAlcohol:(id)sender
{
	// Round slider value
	int roundedValue = roundl(self.sliderAlcohol.value);
	
	// Save data back to .plist
	[PersistentData setLocalData:[NSString stringWithFormat:@"%d", roundedValue] type:@"age_alcohol"];
	
	//NSLog(@"_dataArr: %@",[[PersistentData fetchLocalData] objectForKey:@"age_alcohol"]);
	//NSLog(@"\n");
	
	[self updateSliderAlcohol];
}

- (IBAction)sliderValueChangedTobacco:(id)sender
{
	// Round slider value
	int roundedValue = roundl(self.sliderTobacco.value);
	
	// Save data back to .plist
	[PersistentData setLocalData:[NSString stringWithFormat:@"%d", roundedValue] type:@"age_tobacco"];
	
	//NSLog(@"_dataArr: %@",[[PersistentData fetchLocalData] objectForKey:@"age_tobacco"]);
	//NSLog(@"\n");
	
	[self updateSliderTobacco];
}


-(void)setupColorTheme
{
	// Switch and slider background color
	_swAnimation.onTintColor = [[Colors getSelectedColor] objectAtIndex:1];
	_sliderAlcohol.minimumTrackTintColor = [[Colors getSelectedColor] objectAtIndex:1];
	_sliderTobacco.minimumTrackTintColor = [[Colors getSelectedColor] objectAtIndex:1];
	
	_imgLogo.image = [_imgLogo.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
	_imgLogo.tintColor = [[Colors getSelectedColor] objectAtIndex:1];
	
	[self.navigationController.navigationBar setBarTintColor: [[Colors getSelectedColor] objectAtIndex:1]];
	[self.navigationController.navigationBar setTintColor: [UIColor whiteColor]];
	
	_lblColorTheme.text = [[Colors getSelectedColor] objectAtIndex: 0];
}

#pragma mark - Segment Control (Substance)
-(void)setupSubstanceSegment
{
	_substanceSegment.selectedSegmentIndex = [[[PersistentData fetchLocalData] objectForKey:@"substance_type"] intValue];
}

-(IBAction)segmentValueChanged:(UISegmentedControl *)sender
{
	if (sender == nil) {
		_selectedSubstance = [_substanceSegment titleForSegmentAtIndex:0];
		return;
	}
	switch (sender.selectedSegmentIndex) {
		case 0:
			_selectedSubstance = [sender titleForSegmentAtIndex:sender.selectedSegmentIndex];
			break;
		case 1:
			_selectedSubstance = [sender titleForSegmentAtIndex:sender.selectedSegmentIndex];
			break;
	}

	NSLog(@"Title: %@",[sender titleForSegmentAtIndex:sender.selectedSegmentIndex]);
	NSLog(@"Index: %ld",(long)sender.selectedSegmentIndex);
	
	[PersistentData setLocalData:[NSString stringWithFormat:@"%ld", (long)sender.selectedSegmentIndex] type:@"substance_type"];
}



#pragma mark - Mail composer
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
	switch (result)
	{
		case MFMailComposeResultCancelled:
			NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
			break;
		case MFMailComposeResultSaved:
			NSLog(@"Mail saved: you saved the email message in the drafts folder.");
			break;
		case MFMailComposeResultSent:
			NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
			break;
		case MFMailComposeResultFailed:
			NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
			break;
		default:
			NSLog(@"Mail not sent.");
			break;
	}
	// Remove the mail view
	[self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)openMail:(id)sender
{
	if ([MFMailComposeViewController canSendMail]) {
		MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
		
		mailer.mailComposeDelegate = self;
		mailer.navigationBar.tintColor = [UIColor whiteColor];
		[mailer setSubject:@"Feedback"];
		[mailer setToRecipients:_toRecipients];
		
		NSString *emailBody = @"";
		[mailer setMessageBody:emailBody isHTML:NO];
		
		[self presentViewController:mailer animated:YES completion:nil];
		
	} else {
		
		UIAlertController *alert = [UIAlertController
								 alertControllerWithTitle:@"Failure"
								 message:@"Your device doesn't support the composer sheet"
								 preferredStyle:UIAlertControllerStyleAlert];
		
		[self presentViewController:alert animated:YES completion:nil];
		
		
		UIAlertAction *cancelAction = [UIAlertAction
									   actionWithTitle:NSLocalizedString(@"OK", @"Cancel action")
									   style:UIAlertActionStyleCancel
									   handler:^(UIAlertAction *action)
									   {
										   NSLog(@"Cancel action");
									   }];
		
		[alert addAction:cancelAction];
	}
}





-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
//	if (indexPath.row % 2 == 0) {
//		cell.backgroundColor = [UIColor whiteColor];
//	} else {
//		cell.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.00];
//	}
	
	// remove all insets
	// Remove seperator inset
//	if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//		[cell setSeparatorInset:UIEdgeInsetsZero];
//	}
//	
//	// Prevent the cell from inheriting the Table View's margin settings
//	if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
//		[cell setPreservesSuperviewLayoutMargins:NO];
//	}
//	
//	// Explictly set your cell's layout margins
//	if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//		[cell setLayoutMargins:UIEdgeInsetsZero];
//	}
}


-(IBAction)dmWebsite:(id)sender
{
	[self openExternalLink:@"http://www.defaultmethod.com"];
}

-(IBAction)linkTwitter:(id)sender
{
	[self openExternalLink:@"https://twitter.com/default_method"];
}

-(IBAction)linkFacebook:(id)sender
{
	[self openExternalLink:@"https://www.facebook.com/defaultmethod/"];
}

-(IBAction)linkAppStore:(id)sender
{
	[self openExternalLink:@"https://itunes.apple.com/us/app/check-age/id1138328313?ls=1&mt=8"];
}
- (IBAction)linkPrivacy:(id)sender
{
	[self openExternalLink:@"http://www.defaultmethod.com/privacy.html"];
}

-(void)openExternalLink:(NSString *)externalLink
{
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString: externalLink] options:@{} completionHandler:nil];
}

-(IBAction)tellYourFriends:(id)sender
{
	NSMutableArray *sharingItems = [NSMutableArray new];
	
	NSString *share_text = @"Hey, checkout default_method and the app CheckAge. It's pretty sweet.\n";
	NSString *share_url = @"http://www.defaultmethod.com";
	UIImage *share_img = [UIImage imageNamed:@"dm_logo"];
	
	[sharingItems addObject: share_text];
	[sharingItems addObject: share_url];
	[sharingItems addObject: share_img];
	
	UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
	
	[self presentViewController:activityController animated:YES completion:nil];
}

- (IBAction)enableAnimation:(id)sender
{
	NSString *_switchedState;
	if (_swAnimation.on) {
		_switchedState = @"1";
		NSLog(@"Notification is ON");
	} else {
		_switchedState = @"0";
		NSLog(@"Notification is OFF");
	}

	[PersistentData setLocalData:[NSString stringWithFormat:@"%@", _switchedState] type:@"animation"];
}
@end
