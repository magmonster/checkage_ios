//
//  StartViewController.m
//  CheckAge
//
//  Created by Drew Bombard on 3/12/18.
//  Copyright © 2018 default_method. All rights reserved.
//

#import "StartViewController.h"

@interface StartViewController ()

@end

@implementation StartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
}

-(void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:YES];
	
	//NSLog(@"substance_type: %d", [[[PersistentData fetchLocalData] objectForKey:@"substance_type"] intValue]);
	NSString *selected_substance = @"both";
	switch ([[[PersistentData fetchLocalData] objectForKey:@"substance_type"] intValue]) {
		case 0:
		case 1:
			selected_substance = @"single";
			break;
		case 2:
			selected_substance = @"both";
			break;
		default:
			selected_substance = @"single";
			break;
	}
	
	UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
	MainViewController *main = [storyboard instantiateViewControllerWithIdentifier:selected_substance];
	
	[self.view addSubview:main.view];
	[self addChildViewController:main];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
