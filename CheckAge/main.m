//
//  main.m
//  CheckAge
//
//  Created by Drew Bombard on 10/21/16.
//  Copyright © 2017 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
		
		// Debug values.
		// NOTE: Delete app and rebuild to test out different locale's
		
		//[[NSUserDefaults standardUserDefaults] setObject: [NSArray arrayWithObjects:@"en-gb", nil] forKey:@"AppleLanguages"];
		//[[NSUserDefaults standardUserDefaults] setObject: [NSArray arrayWithObjects:@"en-ca", nil] forKey:@"AppleLanguages"];
		//[[NSUserDefaults standardUserDefaults] setObject: [NSArray arrayWithObjects:@"en-au", nil] forKey:@"AppleLanguages"];
		
		[[NSUserDefaults standardUserDefaults] setObject: [NSArray arrayWithObjects:@"ja-jp", nil] forKey:@"AppleLanguages"];
		
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
