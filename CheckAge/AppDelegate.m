//
//  AppDelegate.m
//  CheckAge
//
//  Created by Drew Bombard on 10/21/16.
//  Copyright © 2017 default_method. All rights reserved.
//

#import "AppDelegate.h"

#if TARGET_OS_IOS
@import Firebase;
#endif


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	#if TARGET_OS_IOS
		// Use Firebase library to configure APIs
		[FIRApp configure];
	#endif
	
	[self defineHeightWidth];
	[self defineDeviceSize];
	
	
	[[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
														  [UIColor whiteColor],
														  NSForegroundColorAttributeName, nil]];
	
	// Override point for customization after application launch.
	return YES;
}


-(void)defineHeightWidth {
	CGRect screenRect = [[UIScreen mainScreen] bounds];
	_screenWidth = screenRect.size.width;
	_screenHeight = screenRect.size.height;
}

-(void)defineDeviceSize {
	
	UIDevice *device = [UIDevice currentDevice];
	//UIDeviceOrientation currentOrientation = device.orientation;
	BOOL isPhone = (device.userInterfaceIdiom == UIUserInterfaceIdiomPhone);
	
	int heightComparison = [[UIScreen mainScreen] bounds].size.height;
	NSLog(@"heightComparison: %d", heightComparison);
	
	if (isPhone == YES) {
		// Do Portrait Phone Things
		switch (heightComparison) {
			case 480:
				NSLog(@"Legacy Phone");
				_isPhoneLegacy = YES;
				break;
			case 568:
				NSLog(@"iPhone 5/iPhone SE");
				_isPhoneLegacy = YES;
				break;
			case 667:
				NSLog(@"iPhone 6/7/8");
				_isPhone6 = YES;
				break;
			case 736:
				NSLog(@"iPhone 6/7/8 Plus");
				_isPhone6Plus = YES;
				break;
			case 812:
				NSLog(@"iPhone X");
				_isPhoneX = YES;
				break;
			default:
				break;
		}
		_deviceHeight = heightComparison;
	}
}


- (void)applicationWillResignActive:(UIApplication *)application {
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
	// Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
