//
//  DateTableViewController.m
//  CheckAge
//
//  Created by Drew Bombard on 4/5/17.
//  Copyright © 2017 default_method. All rights reserved.
//

#import "DateTableViewController.h"

@interface DateTableViewController ()

@end

@implementation DateTableViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
    
	// Uncomment the following line to preserve selection between presentations.
	self.clearsSelectionOnViewWillAppear = NO;
	
	_locale = [DateFormat getLocale];
	
	NSLog(@"date_format: %@", [[PersistentData fetchLocalData] objectForKey:@"date_format"]);
	_dateFormatsArr = @[@"M/d/YY", @"M/d/YYYY", @"M-d-YYYY", @"M.d.YYYY", @"MMM d,YYYY", @"d/M/YY", @"d/M/YYYY", @"d-M-YYYY", @"d.M.YYYY", @"d MMM, YYYY", @"YYYY-M-d"];

	NSIndexPath *indexPath = [NSIndexPath
							  indexPathForRow:[[[PersistentData fetchLocalData] objectForKey:@"date_format_index"] intValue]
							  inSection:[[[PersistentData fetchLocalData] objectForKey:@"date_format_section"] intValue]
							  ];

	[self.tableView selectRowAtIndexPath:indexPath
								animated:YES
						  scrollPosition:UITableViewScrollPositionNone];
	[self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//	NSLog(@"\n\n");
//	NSLog(@"indexPath: %@", indexPath);
//	
//	NSLog(@"indexPath row: %ld", (long)indexPath.row);
//	NSLog(@"indexPath section: %ld", (long)indexPath.section);
//	NSLog(@"indexPath tag: %ld", [tableView cellForRowAtIndexPath:indexPath].tag);
//	
//	NSLog(@"_dateFormatsArr: %@",_dateFormatsArr);
//	NSLog(@"Selected format: %@",[_dateFormatsArr objectAtIndex:[tableView cellForRowAtIndexPath:indexPath].tag]);
//	NSLog(@"\n\n");
	
	[tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
	
	// Save data back to .plist
	[PersistentData setLocalData:[NSString stringWithFormat:@"%ld", (long)indexPath.row] type:@"date_format_index"];
	[PersistentData setLocalData:[NSString stringWithFormat:@"%ld", (long)indexPath.section] type:@"date_format_section"];
	[PersistentData setLocalData:[_dateFormatsArr objectAtIndex:[tableView cellForRowAtIndexPath:indexPath].tag] type:@"date_format"];
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
}


@end
