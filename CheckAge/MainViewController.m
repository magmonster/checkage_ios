//
//  MainViewController.m
//  CheckAge
//
//  Created by Drew Bombard on 10/21/16.
//  Copyright © 2017 default_method. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()

@end


@implementation MainViewController


#pragma mark - Loading Cycles
- (void)viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	
	_appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];

	// Data
	_dataArr = [PersistentData fetchLocalData];
	
	// Country
	_language = [DateFormat getLanguage];
	_locale = [DateFormat getLocale];
	
	// If age minimums haven't been set yet...
	if ([[_dataArr objectForKey:@"default_locale_set"]  isEqual: @"0"]
		|| [_dataArr objectForKey:@"age_alcohol"] == nil
		|| [_dataArr objectForKey:@"age_tobacco"] == nil) {
		[self setMinimumAges];
	}
	
	[[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
	[[NSNotificationCenter defaultCenter]
	 addObserver:self selector:@selector(orientationChanged:)
	 name:UIDeviceOrientationDidChangeNotification
	 object:[UIDevice currentDevice]];
}


-(void)viewWillAppear:(BOOL)animated
{
	_view_background.backgroundColor = [[Colors getSelectedColor] objectAtIndex:1];
}


-(void)viewWillLayoutSubviews
{
	// Remove video player before re-adding it
	[self removeVideoLayer];
	
	[self setupViewPositions];
	
	[self setupSubstanceType];
	
	[self customizeInterface];
}


- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	
	// Remove playerLayer
	[self removeVideoLayer];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	if ([[UIDevice currentDevice] isGeneratingDeviceOrientationNotifications]) {
		[[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
	}
}


-(void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}


- (void)deviceDidRotate:(NSNotification *)notification
{
	_currentDeviceOrientation = [[UIDevice currentDevice] orientation];
	// Do what you want here
}


#pragma mark - Video Playback
- (void)playerItemDidReachEnd:(NSNotification *)notification
{
	AVPlayerItem *p = [notification object];
	[p seekToTime:kCMTimeZero completionHandler:nil];
}


-(void)removeVideoLayer
{
	[playerLayer.player pause];
	[playerLayer removeFromSuperlayer];
}


-(void)addVideoLayer:(NSString *)videoSubstance
{
	if ([[[PersistentData fetchLocalData] objectForKey:@"animation"]  isEqual: @"1"]) {
		NSString *strpath = [[NSBundle mainBundle]pathForResource:videoSubstance ofType:@"mp4"];
		NSURL *url = [NSURL fileURLWithPath:strpath];
		
		AVPlayerItem *item = [AVPlayerItem playerItemWithURL:url];
		player = [AVPlayer playerWithPlayerItem:item];
		
		playerLayer = [AVPlayerLayer playerLayerWithPlayer:player];
		playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
		playerLayer.videoGravity = AVLayerVideoGravityResize;
		playerLayer.frame = _videoView.layer.bounds;
		
		[_videoView.layer addSublayer:playerLayer];
		
		player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(playerItemDidReachEnd:)
													 name:AVPlayerItemDidPlayToEndTimeNotification
												   object:[player currentItem]];
		[player play];
	} else {
		NSLog(@"Disable animation / video background");
	}
}


-(void)debugData
{
	NSLog(@"_dataArr: %@", _dataArr);

	NSLog(@"x type: %f", _substance_container_view.frame.origin.x);
	NSLog(@"y type: %f", _substance_container_view.frame.origin.y);
	NSLog(@"x age: %f", _age_container_view.frame.origin.x);
	NSLog(@"y age: %f", _age_container_view.frame.origin.y);
	NSLog(@"height: %f", _appDelegate.screenHeight);
	NSLog(@"height percent Y: %f", CGRectGetHeight(self.view.bounds) * .175);
}


-(void)setupSubstanceType
{
	// Substance Type(s)
	NSString *_videoSubstance;
	NSString *_txtSubstanceType;
	NSString *_imgSubstanceType;
	NSString *_imgSubstanceTypeParticle;
	
	// Single
	NSString *_txtSubstanceDate;
	NSString *_txtSubstanceMinAge;
	
	NSLog(@"substance_type: %d", [[_dataArr objectForKey:@"substance_type"]  isEqual: @"0"]);
	switch ([[[PersistentData fetchLocalData] objectForKey:@"substance_type"] intValue]) {
		case 0:
			_videoSubstance = @"water_bubbles";
			_txtSubstanceType = @"Alcohol";
			_imgSubstanceType = @"alcohol_whiskey";
			_imgSubstanceTypeParticle = @"bubbles";
			_txtSubstanceDate = [DateFormat getAlcoholMinDate];
			_txtSubstanceMinAge = [[PersistentData fetchLocalData] objectForKey:@"age_alcohol"];
			break;
		case 1:
			_videoSubstance = @"thin_smoke";
			_txtSubstanceType = @"Tobacco";
			_imgSubstanceType = @"smokes";
			_imgSubstanceTypeParticle = @"smoke";
			_txtSubstanceDate = [DateFormat getTobaccoMinDate];
			_txtSubstanceMinAge = [[PersistentData fetchLocalData] objectForKey:@"age_tobacco"];
			break;
		default:
			break;
	}
	_lbl_substance_type.text = _txtSubstanceType;
	_img_substance_type.image = [UIImage imageNamed:_imgSubstanceType];
	_img_substance_particles.image = [UIImage imageNamed:_imgSubstanceTypeParticle];
	
	
	// Positioning Elements
	_yPosSubstanceType = _appDelegate.screenHeight * 0.175;
	_yPosAgeContainer = _appDelegate.screenHeight * 0.627;


	// Resetting back to storyboard positions
	_yPosAgeContainer = _age_container_view.frame.origin.y;
	_yPosSubstanceType = _substance_container_view.frame.origin.y;

	
	[self setViewFrame:_substance_container_view
				  xPos:_substance_container_view.frame.origin.x
				  yPos:_yPosSubstanceType
				 width:_substance_container_view.frame.size.width
				height:_substance_container_view.frame.size.height];
	
	[self setViewFrame:_age_container_view
				  xPos:_age_container_view.frame.origin.x
				  yPos:_yPosAgeContainer
				 width:_age_container_view.frame.size.width
				height:_age_container_view.frame.size.height];
	
	// Single
	_lbl_substance_date.text = _txtSubstanceDate;
	_lbl_substance_min_age.text = _txtSubstanceMinAge;
	
	// Both
	_lbl_alcohol_date.text = [DateFormat getAlcoholMinDate];
	_lbl_tobacco_date.text = [DateFormat getTobaccoMinDate];
	_lbl_alcoholMinAge.text = [[PersistentData fetchLocalData] objectForKey:@"age_alcohol"];
	_lbl_tobaccoMinAge.text = [[PersistentData fetchLocalData] objectForKey:@"age_tobacco"];
	
	
	// Video Background
	[self addVideoLayer:_videoSubstance];
}



# pragma mark - Animation control(s)
- (void)animateView:(NSString *)animationID
		   finished:(NSNumber *)finished
			context:(void *)context
			  delay:(float)delay
		   duration:(float)duration
		 viewToMove:(UIView *)viewToMove
			   xPos:(float)xPos
			   yPos:(float)yPos
{
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:duration];
	[UIView setAnimationDelay:delay];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
	[UIView setAnimationDelegate:self];

	viewToMove.frame = CGRectMake(xPos, yPos, viewToMove.frame.size.width, viewToMove.frame.size.height);
	[UIView commitAnimations];
}


-(void)setViewFrame:(UIView *)viewToSet xPos:(float)xPos yPos:(float)yPos width:(float)width height:(float)height
{
	viewToSet.frame = CGRectMake(xPos, yPos, width, height);
}


-(void)setupViewPositions
{
	//NSLog(@"orientation: %ld", (long)[UIDevice currentDevice].orientation);
	CGFloat screenWidth = CGRectGetWidth(self.view.bounds);
	CGFloat screenHeight = CGRectGetHeight(self.view.bounds);
	
	switch ([UIDevice currentDevice].orientation) {
		case 1:
		case 2:
			// Vertical
			[self setViewFrame:_substance_container_view
						  xPos:0
						  yPos:0
						 width:screenWidth
						height:(screenHeight / 3) * 2];
			
			[self setViewFrame:_age_container_view
						  xPos:0
						  yPos:(screenHeight / 3) * 2
						 width:screenWidth
						height:(screenHeight / 3)];

			[self setViewFrame:_view_age_group
						  xPos:_view_age_group.frame.origin.x
						  yPos: 20
						 width:_view_age_group.frame.size.width
						height:_view_age_group.frame.size.height];
			
			[self setViewFrame:_view_min_age
						  xPos:25
						  yPos:95
						 width:_view_min_age.frame.size.width
						height:_view_min_age.frame.size.height];
			break;
			
		case 3:
		case 4:
			// Horizontal
			[self setViewFrame:_substance_container_view
						  xPos:0
						  yPos:0
						 width:(screenWidth / 2)
						height:screenHeight];
			
			[self setViewFrame:_age_container_view
						  xPos:(screenWidth / 2)
 						  yPos:0
						 width:(screenWidth / 2)
						height:screenHeight];
			
			[self setViewFrame:_view_age_group
						  xPos:_view_age_group.frame.origin.x
						  yPos:(screenHeight / 2) - (_view_age_group.frame.size.height / 2)
						 width:_view_age_group.frame.size.width
						height:_view_age_group.frame.size.height];
			
			[self setViewFrame:_view_min_age
						  xPos:((screenWidth / 2) + ((screenWidth / 2) / 2)) - (_view_min_age.frame.size.width/2)
						  yPos:((screenHeight / 2) - (_view_age_group.frame.size.height / 2)) - (_view_min_age.frame.size.height + 20)
						 width:_view_min_age.frame.size.width
						height:_view_min_age.frame.size.height];
			break;
		default:
			break;
	}
}


-(void) orientationChanged:(NSNotification *)note
{
	UIDevice * device = note.object;
	switch(device.orientation) {
		
		// Portrait
		case UIDeviceOrientationPortrait:
		case UIDeviceOrientationPortraitUpsideDown:
//			_btn_settings.hidden = NO;
			break;
		
		// Landscape
		case UIDeviceOrientationLandscapeRight:
		case UIDeviceOrientationLandscapeLeft:
//			_btn_settings.hidden = YES;
			break;

		case UIDeviceOrientationUnknown:
		case UIDeviceOrientationFaceUp:
		case UIDeviceOrientationFaceDown:
			break;
	}
}


-(void)setMinimumAges
{
	NSLog(@"locale: %@",_locale);
	NSLog(@"_dataArr BEFORE: %@", _dataArr);

	/* Country specific age limits */
	// UK
	if ([_locale isEqual: @"en-gb"]) {
		[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 18] type:@"age_alcohol"];
		[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 18] type:@"age_tobacco"];
	}
	
	//Canada
	if ([_locale isEqual: @"en-ca"]) {
		[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 19] type:@"age_alcohol"];
		[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 19] type:@"age_tobacco"];
	}
	
	//Australia
	if ([_locale isEqual: @"en-au"]) {
		[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 18] type:@"age_alcohol"];
		[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 18] type:@"age_tobacco"];
	}
	
	//Ireland
	if ([_locale isEqual: @"en-ie"]) {
		[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 18] type:@"age_alcohol"];
		[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 18] type:@"age_tobacco"];
	}
	
	// If no specific country defaults set above, default to 18/21 U.S.
	if ([_dataArr objectForKey:@"age_alcohol"] == nil || [_dataArr objectForKey:@"age_tobacco"] == nil) {
		[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 18] type:@"age_alcohol"];
		[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 21] type:@"age_tobacco"];
	}

	
	// Defaults have now been set for first run.
	[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 1] type:@"default_locale_set"];

	
	// Reset the local data arr (in case min's were changed):
	_dataArr = [PersistentData fetchLocalData];
	
	NSLog(@"_dataArr AFTER: %@", _dataArr);
	NSLog(@"\n");
}


-(void)customizeInterface
{
	// Shadow and rounded edges
	[self configShadowLayer:_alcoholAgeBubble
			   cornerRadius:_alcoholAgeBubble.frame.size.width / 2
			  shadowOffsetX:0.0
			  shadowOffsetY:3.0
			   shadowRadius:2.0
					opacity:0.3];
	
	[self configShadowLayer:_tobaccoAgeBubble
			   cornerRadius:_tobaccoAgeBubble.frame.size.width / 2
			  shadowOffsetX:0.0
			  shadowOffsetY:3.0
			   shadowRadius:2.0
					opacity:0.3];
	
	[self configShadowLayer:_view_alcohol
			   cornerRadius:5.0
			  shadowOffsetX:0.0
			  shadowOffsetY:3.0
			   shadowRadius:5.0
					opacity:0.5];
	
	[self configShadowLayer:_view_birthdate
			   cornerRadius:5.0
			  shadowOffsetX:0.0
			  shadowOffsetY:3.0
			   shadowRadius:5.0
					opacity:0.5];
	
	[self configShadowLayer:_view_min_age
			   cornerRadius:_view_min_age.frame.size.width / 2.0
			  shadowOffsetX:0.0
			  shadowOffsetY:3.0
			   shadowRadius:2.0
					opacity:0.2];
}


-(void)configShadowLayer:(UIView *)shadowView
			cornerRadius:(float)cornerRadius
		   shadowOffsetX:(float)shadowOffsetX
		   shadowOffsetY:(float)shadowOffsetY
			shadowRadius:(float)shadowRadius
				 opacity:(float)opacity
{
	CALayer *shadowLayer = shadowView.layer;
	shadowLayer.masksToBounds = NO;
	shadowLayer.cornerRadius = cornerRadius;
	shadowLayer.shadowOffset = CGSizeMake(shadowOffsetX, shadowOffsetY);
	shadowLayer.shadowRadius = shadowRadius;
	shadowLayer.shadowOpacity = opacity;
}


@end
