//
//  MainViewController.m
//  CheckAge
//
//  Created by Drew Bombard on 10/21/16.
//  Copyright © 2017 default_method. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()

@end


@implementation MainViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	
	_appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];

	[self customizeInterface];
	
	// Data
	_dataArr = [PersistentData fetchLocalData];
	
	// Grab country
	_language = [DateFormat getLanguage];
	_locale = [DateFormat getLocale];
	
	// If age minimums haven't been set yet...
	if ([[_dataArr objectForKey:@"default_locale_set"]  isEqual: @"0"]
		|| [_dataArr objectForKey:@"age_alcohol"] == nil
		|| [_dataArr objectForKey:@"age_tobacco"] == nil) {
		[self setMinimumAges];
	}

	
	[[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
	[[NSNotificationCenter defaultCenter]
	 addObserver:self selector:@selector(orientationChanged:)
	 name:UIDeviceOrientationDidChangeNotification
	 object:[UIDevice currentDevice]];

	
	// [self debugData];
}


-(void)viewWillAppear:(BOOL)animated
{
	_lbl_alcohol_date.text = [DateFormat getAlcoholMinDate];
	_lbl_tobacco_date.text = [DateFormat getTobaccoMinDate];
	
	_lbl_alcoholMinAge.text = [[PersistentData fetchLocalData] objectForKey:@"age_alcohol"];
	_lbl_tobaccoMinAge.text = [[PersistentData fetchLocalData] objectForKey:@"age_tobacco"];
}

- (void)deviceDidRotate:(NSNotification *)notification
{
	_currentDeviceOrientation = [[UIDevice currentDevice] orientation];
	// Do what you want here
	
	NSLog(@"orientation: %ld", (long)_currentDeviceOrientation);
}

- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	if ([[UIDevice currentDevice] isGeneratingDeviceOrientationNotifications]) {
		[[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
	}
}


- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}


-(UIStatusBarStyle)preferredStatusBarStyle {
	return UIStatusBarStyleLightContent;
}


// Override to allow orientations other than the default portrait orientation.
-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations
	//return (interfaceOrientation == UIInterfaceOrientationPortrait);
	return YES;
}



- (void) orientationChanged:(NSNotification *)note {
	
	UIDevice * device = note.object;
	
	switch(device.orientation) {
		case UIDeviceOrientationPortrait:
			_btn_info.hidden = NO;
			break;
			
		case UIDeviceOrientationPortraitUpsideDown:
			_btn_info.hidden = YES;
			break;
		
		case UIDeviceOrientationLandscapeRight:
			NSLog(@"\n\nHide the settings button.\n\n");
			_btn_info.hidden = YES;
			break;
			
		case UIDeviceOrientationLandscapeLeft:
			NSLog(@"\n\nHide the settings button.\n\n");
			_btn_info.hidden = YES;
			break;
		
		default:
			break;
	};
}


-(void)setMinimumAges
{
	NSLog(@"locale: %@",_locale);
	NSLog(@"_dataArr BEFORE: %@", _dataArr);

	
	/* Country specific age limits */
	// UK
	if ([_locale isEqual: @"en-gb"]) {
		[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 18] type:@"age_alcohol"];
		[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 18] type:@"age_tobacco"];
	}
	
	//Canada
	if ([_locale isEqual: @"en-ca"]) {
		[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 19] type:@"age_alcohol"];
		[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 19] type:@"age_tobacco"];
	}
	
	//Australia
	if ([_locale isEqual: @"en-au"]) {
		[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 18] type:@"age_alcohol"];
		[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 18] type:@"age_tobacco"];
	}
	
	//Ireland
	if ([_locale isEqual: @"en-ie"]) {
		[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 18] type:@"age_alcohol"];
		[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 18] type:@"age_tobacco"];
	}
	
	// If no specific country defaults set above, default to 18/21 U.S.
	if ([_dataArr objectForKey:@"age_alcohol"] == nil || [_dataArr objectForKey:@"age_tobacco"] == nil) {
		[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 18] type:@"age_alcohol"];
		[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 21] type:@"age_tobacco"];
	}

	
	// Defaults have now been set for first run.
	[PersistentData setLocalData:[NSString stringWithFormat:@"%d", 1] type:@"default_locale_set"];

	
	// Reset the local data arr (in case min's were changed):
	_dataArr = [PersistentData fetchLocalData];
	
	NSLog(@"_dataArr AFTER: %@", _dataArr);
	NSLog(@"\n");
}


-(void)customizeInterface
{
	// Shadow and rounded edges
	CALayer *alcoholAgeRoundShadowLayer = _alcoholAgeBubble.layer;
	alcoholAgeRoundShadowLayer.masksToBounds = NO;
	alcoholAgeRoundShadowLayer.cornerRadius = _alcoholAgeBubble.frame.size.width/2;
	alcoholAgeRoundShadowLayer.shadowOffset = CGSizeMake(0.0f, 1.5f);
	alcoholAgeRoundShadowLayer.shadowRadius = 2.0;
	alcoholAgeRoundShadowLayer.shadowOpacity = 0.3;
	
	CALayer *tobaccoAgeRoundShadowLayer = _tobaccoAgeBubble.layer;
	tobaccoAgeRoundShadowLayer.masksToBounds = NO;
	tobaccoAgeRoundShadowLayer.cornerRadius = _tobaccoAgeBubble.frame.size.width/2;
	tobaccoAgeRoundShadowLayer.shadowOffset = CGSizeMake(0.0f, 1.5f);
	tobaccoAgeRoundShadowLayer.shadowRadius = 2.0;
	tobaccoAgeRoundShadowLayer.shadowOpacity = 0.3;
	
	
	_view_alcohol_label.layer.cornerRadius = 6;
	_view_alcohol_label.layer.masksToBounds = YES;
	
	_view_tobacco_label.layer.cornerRadius = 6;
	_view_tobacco_label.layer.masksToBounds = YES;
	
	CALayer *alcoholRoundShadowLayer = _view_alcohol.layer;
	alcoholRoundShadowLayer.masksToBounds = NO;
	alcoholRoundShadowLayer.cornerRadius = 5.0;
	alcoholRoundShadowLayer.shadowOffset = CGSizeMake(0.0f, 3.0f);
	alcoholRoundShadowLayer.shadowRadius = 5.0;
	alcoholRoundShadowLayer.shadowOpacity = 0.5;
		
	CALayer *tobaccoRoundShadowLayer = _view_tobacco.layer;
	tobaccoRoundShadowLayer.masksToBounds = NO;
	tobaccoRoundShadowLayer.cornerRadius = 5.0;
	tobaccoRoundShadowLayer.shadowOffset = CGSizeMake(0.0f, 3.0f);
	tobaccoRoundShadowLayer.shadowRadius = 5.0;
	tobaccoRoundShadowLayer.shadowOpacity = 0.5;
}








@end
