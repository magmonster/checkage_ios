//
//  InfoReferencesViewController.m
//  CheckAge
//
//  Created by Drew Bombard on 5/4/18.
//  Copyright (c) 2018 default_method. All rights reserved.
//

#import "InfoReferencesViewController.h"

@interface InfoReferencesViewController ()

@end

@implementation InfoReferencesViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	
	[self customizeInterface];
	
	_url =[[NSBundle mainBundle] bundleURL];
	_htmlFile = [[NSBundle mainBundle] pathForResource:@"references" ofType:@"html"];
	_htmlString = [NSString stringWithContentsOfFile:_htmlFile encoding:NSUTF8StringEncoding error:nil];
	
	self.webview.delegate = self;
	[_webview loadHTMLString:_htmlString baseURL:_url];
}


-(void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}


-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType
{
	if ( inType == UIWebViewNavigationTypeLinkClicked ) {
		UIApplication *application = [UIApplication sharedApplication];
		[application openURL:[inRequest URL] options:@{} completionHandler:nil];
		return NO;
	}
	
	return YES;
}


-(void)customizeInterface
{
	self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
}

@end
