//
//  DateTableViewController.h
//  CheckAge
//
//  Created by Drew Bombard on 4/5/17.
//  Copyright © 2017 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>

// Utilities
#import "Colors.h"
#import "DateFormat.h"

// Data
#import "PersistentData.h"

@interface DateTableViewController : UITableViewController

@property (strong, nonatomic) NSString *locale;
@property (strong, nonatomic) NSArray *dateFormatsArr;
@property (strong, nonatomic) IBOutlet UITableView *dateTableView;

@property (strong, nonatomic) IBOutlet UITableViewCell *cellMDY0;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellMDY1;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellMDY2;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellMDY3;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellMDY4;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellDMY0;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellDMY1;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellDMY2;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellDMY3;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellDMY4;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellYMD0;

@end
