//
//  AppDelegate.h
//  CheckAge
//
//  Created by Drew Bombard on 10/21/16.
//  Copyright © 2017 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>

@import Firebase;


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (assign) CGFloat deviceHeight;
@property (assign) CGFloat screenHeight;
@property (assign) CGFloat screenWidth;

@property (assign) BOOL isPhoneLegacy;
@property (assign) BOOL isPhone5;
@property (assign) BOOL isPhone6;
@property (assign) BOOL isPhone6Plus;
@property (assign) BOOL isPhoneX;

@end

