//
//  AgeLimits.h
//  
//
//  Created by Drew Bombard on 1/25/17.
//
//

#import <Foundation/Foundation.h>

@interface AgeLimits : NSObject

@property (strong, nonatomic) NSString *language;
@property (nonatomic, assign) int min_alcohol_age;
@property (nonatomic, assign) int min_tobacco_age;

@end
