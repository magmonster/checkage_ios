//
//  DateFormat.m
//  CheckAge
//
//  Created by Drew Bombard on 4/6/17.
//  Copyright © 2017 default_method. All rights reserved.
//

#import "DateFormat.h"

@implementation DateFormat


#pragma mark - Formatting Utilities
+(NSDateFormatter *)dateFormatter
{
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	dateFormatter.timeStyle = NSDateFormatterNoStyle;
	[dateFormatter setDateFormat:[[PersistentData fetchLocalData] objectForKey:@"date_format"]];

	return dateFormatter;
}

+(NSDate *)alcoholMinDate
{
	NSDate *now = [NSDate date];
	NSDateComponents *minusAlcoholYears = [NSDateComponents new];
	minusAlcoholYears.year = - [[[PersistentData fetchLocalData] objectForKey:@"age_alcohol"] intValue];
	NSDate *alcoholMinDate = [[NSCalendar currentCalendar] dateByAddingComponents:minusAlcoholYears
																		   toDate:now
																		  options:0];
	
	NSDateFormatter *dateFormatterNoTime = [[NSDateFormatter alloc] init];
	[dateFormatterNoTime setDateFormat:@"yyyy-mm-dd"];
	NSString *convertedString = [dateFormatterNoTime stringFromDate:alcoholMinDate];

	
	
	
	NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
	[dateFormatter1 setDateFormat:@"yyyy-M-dd"];
	NSLog(@"myDate1: %@", [dateFormatter1 stringFromDate:alcoholMinDate]);

	
	NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
	[dateFormatter2 setDateFormat:@"yyyy / MM /dd"];
	
	NSDateFormatter *dateFormatter3 = [[NSDateFormatter alloc] init];
	[dateFormatter3 setDateFormat:@"MMM d,YYYY"];


	NSLog(@"\n\n");
	NSLog(@"alcoholMinDate: %@", alcoholMinDate);
	NSLog(@"convertedString: %@", convertedString);
	
	NSLog(@"myDate1: %@", [dateFormatter1 stringFromDate:alcoholMinDate]);
	NSLog(@"myDate2: %@", [dateFormatter2 stringFromDate:alcoholMinDate]);
	NSLog(@"myDate3: %@", [dateFormatter3 stringFromDate:alcoholMinDate]);
	
	NSLog(@"\n\n");
	
	return alcoholMinDate;
}

+(NSString *)curentDateStringFromDate:(NSDate *)dateTimeInLine withFormat:(NSString *)dateFormat
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
	[formatter setDateFormat:dateFormat];
	NSString *convertedString = [formatter stringFromDate:dateTimeInLine];
	
	return convertedString;
}


+(NSDate *)tobaccoMinDate
{
	NSDate *now = [NSDate date];
	NSDateComponents *minusTobaccoYears = [NSDateComponents new];
	minusTobaccoYears.year = - [[[PersistentData fetchLocalData] objectForKey:@"age_tobacco"] intValue];
	NSDate *tobaccoMinDate = [[NSCalendar currentCalendar] dateByAddingComponents:minusTobaccoYears
																		   toDate:now
																		  options:0];
	return tobaccoMinDate;
}



#pragma mark - Getters
+(NSString *)getLanguage
{
	return [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
}

+(NSString *)getLocale
{
	// DEBUG
	return [[NSLocale autoupdatingCurrentLocale] objectForKey: NSLocaleCountryCode];
}

+(NSString *)getCurrentYear
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"yyyy"];
	return [formatter stringFromDate:[NSDate date]];
}

+(NSString *)getTodayDate
{
	NSDate *now = [NSDate date];
	return [[self dateFormatter] stringFromDate:now];
}

+(NSString *)getAlcoholMinDate
{
	return [[self dateFormatter] stringFromDate:[self alcoholMinDate]];
}

+(NSString *)getTobaccoMinDate
{
	return [[self dateFormatter] stringFromDate:[self tobaccoMinDate]];
}


@end
