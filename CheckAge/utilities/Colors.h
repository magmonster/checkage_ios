//
//  Colors.h
//  CheckAge
//
//  Created by Drew Bombard on 5/7/18.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

#import "PersistentData.h"

@interface Colors: NSObject

// Background color arrays
@property (nonatomic,readonly) NSArray *arrLime;
@property (nonatomic,readonly) NSArray *arrMint;
@property (nonatomic,readonly) NSArray *arrGrass;

@property (nonatomic,readonly) NSArray *arrLemon;
@property (nonatomic,readonly) NSArray *arrButter;

@property (nonatomic,readonly) NSArray *arrRust;

@property (nonatomic,readonly) NSArray *arrFire;
@property (nonatomic,readonly) NSArray *arrPumpkin;
@property (nonatomic,readonly) NSArray *arrOrange;


@property (nonatomic,readonly) NSArray *arrCrimson;
@property (nonatomic,readonly) NSArray *arrCardinal;
@property (nonatomic,readonly) NSArray *arrCarmine;

@property (nonatomic,readonly) NSArray *arrGrape;
@property (nonatomic,readonly) NSArray *arrViolet;
@property (nonatomic,readonly) NSArray *arrDeepPurple;

@property (nonatomic,readonly) NSArray *arrBlue;
@property (nonatomic,readonly) NSArray *arrBlue2;
@property (nonatomic,readonly) NSArray *arrDeepBlue;
@property (nonatomic,readonly) NSArray *arrDarkBlue;

@property (nonatomic,readonly) NSArray *arrGray20;
@property (nonatomic,readonly) NSArray *arrGray35;
@property (nonatomic,readonly) NSArray *arrGray50;
@property (nonatomic,readonly) NSArray *arrDarkGray;
@property (nonatomic,readonly) NSArray *arrCharcoal;
@property (nonatomic,readonly) NSArray *arrBlack;


// All Colors
@property (nonatomic,readonly) NSArray *all_colors_arr;

// Text Colors
@property (nonatomic,readonly) UIColor *txtDark;
@property (nonatomic,readonly) UIColor *txtLight;


// Navigation bar & text (if separate)
@property (nonatomic,readonly) UIColor *navigationBar;
@property (nonatomic,readonly) UIColor *navigationBarText;


+(Colors*)get;
+(int)getSelectedColorIndex;
+(NSArray *)getSelectedColor;
+(UIColor*)colorWithHexString:(NSString*)hex;

@end
