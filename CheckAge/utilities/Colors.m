//
//  Colors.m
//  CheckAge
//
//  Created by Drew Bombard on 5/7/18.
//  Copyright (c) 2013 default_method. All rights reserved.
//

#import "Colors.h"

@implementation Colors

-(id)init
{
	self = [super init];
	if (self) {
		
		NSNull *nullValue = [NSNull null];
		
	
		// Text Color(s)
		_txtDark = [Colors colorWithHexString:@"333333"];
		_txtLight = [Colors colorWithHexString:@"FFFFFF"];
		
		
		// Background colors:
		// Greens
		_arrLime		= @[@"Lime", [Colors colorWithHexString:@"9BC53D"], _txtLight, @1, nullValue];
		_arrMint		= @[@"Mint", [Colors colorWithHexString:@"4CAF50"], _txtLight, @1, nullValue];
		_arrGrass		= @[@"Grass", [Colors colorWithHexString:@"4E8E47"], _txtLight, @1, nullValue];
		
		//Yellows
		_arrButter		= @[@"Butter", [Colors colorWithHexString:@"FEDc56"], _txtDark, @0, nullValue];
		_arrLemon		= @[@"Lemon", [Colors colorWithHexString:@"F5E749"], _txtDark, @0, nullValue];
		_arrRust		= @[@"Rust", [Colors colorWithHexString:@"F06550"], _txtLight, @0, nullValue];
		
		// Oranges
		_arrFire		= @[@"Fire", [Colors colorWithHexString:@"FDA50F"], _txtLight, @1, nullValue];
		_arrPumpkin		= @[@"Pumpkin", [Colors colorWithHexString:@"FD8608"], _txtLight, @1, nullValue];
		_arrOrange		= @[@"Orange", [Colors colorWithHexString:@"FD6600"], _txtLight, @1, nullValue];
		
		
		// Reds
		_arrCrimson		= @[@"Crimson", [Colors colorWithHexString:@"E9193A"], _txtLight, @1, nullValue];
		_arrCardinal	= @[@"Cardinal", [Colors colorWithHexString:@"D31735"], _txtLight, @1, nullValue];
		_arrCarmine		= @[@"Carmine", [Colors colorWithHexString:@"B8122D"], _txtLight, @1, nullValue];
		
		// Purple
		_arrGrape		= @[@"Grape", [Colors colorWithHexString:@"B067B0"], _txtLight, @1, nullValue];
		_arrViolet		= @[@"Violet", [Colors colorWithHexString:@"712171"], _txtLight, @1, nullValue];
		_arrDeepPurple	= @[@"Deep Purple", [Colors colorWithHexString:@"531A53"], _txtLight, @1, nullValue];
		
		// Blues
		_arrBlue		= @[@"Blue", [Colors colorWithHexString:@"0A2B4B"], _txtLight, @1, nullValue];
		_arrDeepBlue	= @[@"Deep Blue", [Colors colorWithHexString:@"094074"], _txtLight, @1, nullValue];
		_arrDarkBlue	= @[@"Dark Blue", [Colors colorWithHexString:@"0A2B4B"], _txtLight, @1, nullValue];
		
		// Black and Grays
		_arrGray20		= @[@"Gray 15%", [Colors colorWithHexString:@"D9D9D9"], _txtDark, @0, nullValue];
		_arrGray35		= @[@"Gray 35%", [Colors colorWithHexString:@"#A0A0A0"], _txtLight, @1, nullValue];
		_arrGray50		= @[@"Gray 50%", [Colors colorWithHexString:@"666666"], _txtLight, @1, nullValue];
		_arrDarkGray	= @[@"Dark Gray", [Colors colorWithHexString:@"595959"], _txtLight, @1, nullValue];
		_arrCharcoal	= @[@"Charcoal", [Colors colorWithHexString:@"333333"], _txtLight, @1, nullValue];
		_arrBlack		= @[@"Black", [Colors colorWithHexString:@"0C0C0C"], _txtLight, @1, nullValue];
		
		_all_colors_arr = @[_arrLime
							,_arrMint
							,_arrGrass
							,_arrButter
							,_arrLemon
							,_arrRust
							,_arrFire
							,_arrPumpkin
							,_arrOrange
							,_arrCrimson
							,_arrCardinal
							,_arrCarmine
							,_arrGrape
							,_arrViolet
							,_arrDeepPurple
							,_arrBlue
							,_arrDeepBlue
							,_arrDarkBlue
							,_arrGray20
							,_arrGray35
							,_arrGray50
							,_arrDarkGray
							,_arrCharcoal
							,_arrBlack
							];
	}
	return self;
}

+(Colors *)get
{
	static Colors* colors = nil;
	if (!colors) {
		colors = [[Colors alloc] init];
	}
	return colors;
}

#pragma mark - Selected Colors
+(int)getSelectedColorIndex
{
	return [[[PersistentData fetchLocalData] objectForKey:@"background_color"] intValue];
}


+(NSArray *)getSelectedColor
{
	//NSLog(@"colors_array: %@", [[Colors get].all_colors_arr objectAtIndex: [self getSelectedColorIndex]]);
	return [[Colors get].all_colors_arr objectAtIndex: [self getSelectedColorIndex]];
}


+(UIColor *)colorWithHexString:(NSString *) hex
{
	// Performs conversion from hex string to color.
	NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
	
	// String should be 6 or 8 characters
	if ([cString length] < 6) return [UIColor grayColor];
	
	// strip 0X if it appears
	if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
	
	if ([cString length] != 6) return  [UIColor grayColor];
	
	// Separate into r, g, b substrings
	NSRange range;
	range.location = 0;
	range.length = 2;
	NSString *rString = [cString substringWithRange:range];
	
	range.location = 2;
	NSString *gString = [cString substringWithRange:range];
	
	range.location = 4;
	NSString *bString = [cString substringWithRange:range];
	
	// Scan values
	unsigned int r, g, b;
	[[NSScanner scannerWithString:rString] scanHexInt:&r];
	[[NSScanner scannerWithString:gString] scanHexInt:&g];
	[[NSScanner scannerWithString:bString] scanHexInt:&b];
	
	return [UIColor colorWithRed:((float) r / 255.0f)
						   green:((float) g / 255.0f)
							blue:((float) b / 255.0f)
						   alpha:1.0f];
}

@end
