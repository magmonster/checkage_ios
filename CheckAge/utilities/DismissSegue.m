//
//  DismissSegue.m
//  CheckAge
//
//  Created by Drew Bombard on 3/12/18.
//  Copyright © 2018 default_method. All rights reserved.
//

#import "DismissSegue.h"

@implementation DismissSegue

-(void)perform
{
	UIViewController *sourceViewController = self.sourceViewController;
	[sourceViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
