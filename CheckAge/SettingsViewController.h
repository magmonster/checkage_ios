//
//  SettingsViewController.h
//  CheckAge
//
//  Created by Drew Bombard on 10/21/16.
//  Copyright © 2017 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "StartViewControlleriPad.h"

// Utilities
#import "Colors.h"
#import "AppStats.h"

@interface SettingsViewController : UIViewController


@property (strong, nonatomic) IBOutlet UIView *containerView;

// Actions
-(IBAction)dismissModal:(id)sender;

@end
