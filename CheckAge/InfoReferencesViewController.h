//
//  InfoReferencesViewController.h
//  CheckAge
//
//  Created by Drew Bombard on 5/4/18.
//  Copyright (c) 2018 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebKit/WebKit.h"

@interface InfoReferencesViewController : UIViewController <UIWebViewDelegate, WKUIDelegate>

@property (strong, nonatomic) NSURL *url;
@property (strong, nonatomic) NSString *htmlFile;
@property (strong, nonatomic) NSString *htmlString;
@property (strong, nonatomic) IBOutlet UIWebView *webview;

@end
