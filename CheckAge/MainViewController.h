//
//  MainViewController.h
//  CheckAge
//
//  Created by Drew Bombard on 10/21/16.
//  Copyright © 2017 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>

#import "Colors.h"
#import "DateFormat.h"
#import "PersistentData.h"


@interface MainViewController : UIViewController {
	AVPlayer *player;
	AVPlayerLayer *playerLayer;
}

@property (strong, nonatomic) AppDelegate *appDelegate;

@property (assign, nonatomic) int yPosSubstanceType;
@property (assign, nonatomic) int yPosAgeContainer;

@property (strong, nonatomic) NSString *locale;
@property (strong, nonatomic) NSString *language;
@property (weak, nonatomic) IBOutlet UIView *videoView;
@property (strong, nonatomic) NSMutableDictionary *dataArr;
@property (strong, nonatomic) IBOutlet UIView *alcoholAgeBubble;
@property (strong, nonatomic) IBOutlet UILabel *lbl_alcoholMinAge;
@property (strong, nonatomic) IBOutlet UIView *tobaccoAgeBubble;
@property (strong, nonatomic) IBOutlet UILabel *lbl_tobaccoMinAge;


@property (nonatomic) UIDeviceOrientation currentDeviceOrientation;

// Shared
@property (strong, nonatomic) IBOutlet UIView *view_alcohol;
@property (strong, nonatomic) IBOutlet UIView *age_container_view;
@property (strong, nonatomic) IBOutlet UIView *view_birthdate;
@property (strong, nonatomic) IBOutlet UIButton *btn_settings;
@property (strong, nonatomic) IBOutlet UIView *view_background;

// Both
@property (strong, nonatomic) IBOutlet UIView *view_alcohol_label;
@property (strong, nonatomic) IBOutlet UIView *view_tobacco_label;
@property (strong, nonatomic) IBOutlet UILabel *lbl_alcohol_date;
@property (strong, nonatomic) IBOutlet UILabel *lbl_tobacco_date;

// Single
@property (strong, nonatomic) IBOutlet UIView *view_min_age;
@property (strong, nonatomic) IBOutlet UIView *substance_container_view;
@property (strong, nonatomic) IBOutlet UILabel *lbl_substance_date;
@property (strong, nonatomic) IBOutlet UILabel *lbl_substance_min_age;
@property (strong, nonatomic) IBOutlet UILabel *lbl_substance_type;
@property (strong, nonatomic) IBOutlet UIImageView *img_substance_type;
@property (strong, nonatomic) IBOutlet UIImageView *img_substance_particles;


@property (strong, nonatomic) IBOutlet UIView *view_age_group;


@end
