//
//  PersistentData.m
//  
//
//  Created by Drew Bombard on 3/1/17.
//
//

#import "PersistentData.h"

@implementation PersistentData

-(id)init {
	self = [super init];
	if (self) {

		blah = @"ssdfadsfadsf";
	}
	return self;
}


+(NSMutableDictionary *)fetchLocalData
{
	NSError *error;
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:@"Data.plist"];
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
 
	if (![fileManager fileExistsAtPath: path]) {
		NSString *bundle = [[NSBundle mainBundle] pathForResource:@"Data" ofType:@"plist"];
		[fileManager copyItemAtPath:bundle toPath: path error:&error];
	}

	NSMutableDictionary *dataArr = [[NSMutableDictionary alloc] initWithContentsOfFile: path];

	NSLog(@"_dataArr: %@",dataArr);
	NSLog(@"\n");
	return dataArr;
}

+(void)setLocalData:(NSString *)age type:(NSString *)type
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:@"Data.plist"];
	
	NSMutableDictionary *dataArr = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
	
	[dataArr setObject:age forKey:type];
	[dataArr writeToFile: path atomically:YES];
	
	NSLog(@"Saved %@ to %@:", age, type);
	NSLog(@"\n");
}


@end
