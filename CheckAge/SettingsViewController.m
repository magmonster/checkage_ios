//
//  SettingsViewController.m
//  CheckAge
//
//  Created by Drew Bombard on 10/21/16.
//  Copyright © 2017 default_method. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController


#pragma mark - App Setup
- (void)viewDidLoad
{
    [super viewDidLoad];
	
	[[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
}


#pragma mark - Button Actions
- (IBAction)dismissModal:(id)sender
{
	[self performSegueWithIdentifier:@"dismissModal" sender:self];
	[self dismissViewControllerAnimated:YES completion:nil];
	
}


@end
