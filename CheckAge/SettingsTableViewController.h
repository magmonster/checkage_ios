//
//  SettingsTableViewController.h
//  CheckAge
//
//  Created by Drew Bombard on 10/21/16.
//  Copyright © 2017 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

// Utilities
#import "Colors.h"
#import "AppStats.h"
#import "DateFormat.h"
#import "AppDelegate.h"

// Data
#import "PersistentData.h"


@interface SettingsTableViewController : UITableViewController <MFMailComposeViewControllerDelegate>


// Outlets
@property (strong, nonatomic) NSArray *toRecipients;
@property (weak, nonatomic) IBOutlet UILabel *lblColorTheme;

@property (strong, nonatomic) IBOutlet UILabel *lblCopyright;
@property (strong, nonatomic) IBOutlet UILabel *lblVersionNum;
@property (strong, nonatomic) IBOutlet UIImageView *imgLogo;

@property (strong, nonatomic) IBOutlet UILabel *lblDateFormat;
@property (strong, nonatomic) IBOutlet UISlider *sliderAlcohol;
@property (strong, nonatomic) IBOutlet UISlider *sliderTobacco;
@property (strong, nonatomic) IBOutlet UILabel *lblSliderTobacco;
@property (strong, nonatomic) IBOutlet UILabel *lblSliderAlchohol;

@property (strong, nonatomic) NSString *selectedSubstance;
@property (strong, nonatomic) IBOutlet UISegmentedControl *substanceSegment;

-(IBAction)enableAnimation:(id)sender;
@property (strong, nonatomic) IBOutlet UISwitch *swAnimation;



// Local Data
@property (strong, nonatomic) NSString *locale;
@property (strong, nonatomic) NSMutableDictionary *dataArr;


@end
